# Bernardo's README.md

Hi! 👋 
My name is Bernardo and I am Engineering Manager, working with the anti-money laundering team at Itaú Unibanco. 

I lead a team of more than 30 engineers with the purpose of positively impacting the Itaú ecosystem and society through our authentication systems.

I am an internet systems specialist, focusing my practices and learning around DevSecOPS engineering to help my teams deliver software with operational excellence, security, reliability, performance efficiency and cost optimization.

Lifelong learner uncovering better ways of developing software by doing it and helping others do it.

## About me

A mix of personal trivial, my strengths and weaknesses:

* I was born and raised in [Rio de Janeiro](https://pt.wikipedia.org/wiki/Rio_de_Janeiro) and currently live in [São Paulo](https://pt.wikipedia.org/wiki/S%C3%A3o_Paulo) with my [wife](https://www.instagram.com/danifsantos.br/) and our dog [Code](https://www.instagram.com/_brbernardo/). My time zone is [UTC -3](https://time.is/S%C3%A3o_Paulo).
* I believe in [collaboration](https://about.gitlab.com/handbook/values/#collaboration) and [transparency](https://about.gitlab.com/handbook/values/#transparency) above all.
* I am more of a realist than a visionary.
* I am impacient and I do not ask for permission. I turn ideas into actionable items and I will make sure you are involved.
* I am strong with program planning. I don't usually take notes during meetings ✍️
* I have a strong sense of ownership when it comes to work. Related to that, I do not like inefficiency and will look for any way to improve that.
* I value stability and predictablity - I hate feeling uncomfortable. Sometimes I can react negatively to change but I do my best to embrace it and keep a positive mindset. I recognize that growing means feeling uncomfortable.
* English is not my native language which can affect how I verbally express myself. I experience a lot of non-linear thinking in a mixture of Portuguese and English, which reflects in my choice of words or sentence structure. In other words, I think as I speak.

### Personality tests

* [Social Style](https://about.gitlab.com/handbook/leadership/emotional-intelligence/social-styles/) - [Analytical] and sometimes [Amiable].
* [INTJ-T](https://www.16personalities.com) - [Architect](https://www.16personalities.com/intj-personality) and [Commander](https://www.16personalities.com/entj-personality): independent to the core, want to shake off other people’s expectations and pursue their own ideas.
* [Enneagram](https://www.enneagraminstitute.com/type-descriptions) - [Challenger](https://www.enneagraminstitute.com/type-8): the sensitive, introspective type: expressive, dramatic, self-absorbed, and temperamental.

### Personal principles

1. Allow events to change you and your craft.
1. [Commitment](https://www.scrumalliance.org/about-scrum/values), [courage](https://www.scrumalliance.org/about-scrum/values), [focus](https://www.scrumalliance.org/about-scrum/values), [openness](https://www.scrumalliance.org/about-scrum/values) and [respect](https://www.scrumalliance.org/about-scrum/values).
1. Tell the real story.
1. Failure is inevitable.
1. Respect your own limitations.
1. Remember to have fun.

### Communication style

My preferred method of communication is non-verbalI am very talkative and expressive and you will see that this translated into my writing. I also like learning through narrative, so sometimes I will send you an invite to a Teams call if I need you to explain things to me.

I ask questions. I am not afraid of not knowing things, and I will seek help whenever necessary. If it feels like I am questioning you it is because I am either: a) trying to gather context or, b) trying to understand if everyone is on the same page.

### Relationships

I take pride in the relationships I build at work. I aim to have an authentic, open, trusting relationship with you. I will not lie to you about how I feel, or what is going on inside my head. I will work towards building a sense of companionship, so please reach out if you want to chat!

I assume you are very good at your job, and that you are here to deliver great value to our company and people. You are the expert as well. I will work to provide necessary context and ask questions to help you get your ideas but I will not override you.

You will let me know if I am blocking you. One of my main responsibilities is ensuring that we are set up for success. Please reach out and let me know how I can help.

## Feedback

I love feedback — it is critical to my success here. I prefer to give and receive written feedback (async), so the information can be first digested, to be then discussed together (sync). You will find me asking for ongoing feedback on things I can do better, things I can improve, and things I am doing well. I want it to be a two-way street. If you are ever surprised by any feedback from me on your reviews then I have not done my job right.

## 1:1 meetings

I value connecting with my teammates regularly. I am a fan of 1:1s, and also love to experience the almost "in-person" interactions of a video call 😊.
As a mentor/manager, I believe 1:1s are for you to set the agenda. What would you like to talk about? What is going well? What is bugging you? How can I help you? If there are things that I want to ask you, I will do it, but this is your time. If I have feedback, I will also provide it during this time. 

By default, you should schedule a bi-weekly, 30 minute conversation with me. I prefer that you schedule the meeting, as you can pick a time that works for your time.

## Logistics

### Work hours

* I generally work from ~ 09:00 to ~18:00 [UTC -3](https://time.is/S%C3%A3o_Paulo).
* You will see my calendar blocked with a Busy event whenever I take personal time during the day.


### How to reach me

* **Teams, Slack**: DM or mention is probably the best way to reach me during the day. I will always have Teams and Slack open during business hours. I avoid checking Teams on my personal phone, so I will respond once I am back to the computer.
* **GitLab**: Please @ me directly in comments so I see your message, and assign MRs to me. I use email for my GitLab to-dos.
* **E-mail**: I read my emails multiple times a day. You should not feel obligated to read or respond to any of the emails you receive from me outside of your normal hours.
* **Text message/WhatsApp, phone call**: I rather not be contacted on text message/WhatsApp or phone call, unless it is an emergency.

### Scheduling

* If you see a good time on my calendar inside my working hours, please schedule something (no need to ask first).
* If you need to reschedule something, go ahead! All my calendar events are set with modified privileges and you should feel free to go ahead and move it to some other time slot that is within business hours and not conflicting with other meetings.

## Footnotes

This is not meant to be a static file, and its contents will change as I learn how to navigate through life at GitLab. I would love your feedback! Did you find the time you spent reading this valuable? Was something critical missing? Feel free to go ahead and submit an MR to this document.

If you see me not living up to anything included please let me know 😉. It is possible that I have changed my mind, or that I am just dropping the ball. Either way, get in touch!

